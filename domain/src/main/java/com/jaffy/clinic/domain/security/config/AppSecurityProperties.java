package com.jaffy.clinic.domain.security.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "security.properties.jwt")
public class AppSecurityProperties {
    private String jwtSecret;
    private Long jwtExpirationTime;
}