package com.jaffy.clinic.domain.appointment.service;

import com.jaffy.clinic.ports.appointment.model.Appointment;
import com.jaffy.clinic.ports.appointment.repository.IAppointmentRepository;
import com.jaffy.clinic.ports.appointment.service.IAppointmentService;
import com.jaffy.clinic.ports.common.exception.NotFoundException;
import com.jaffy.clinic.ports.common.exception.ValidationFailedException;
import com.jaffy.clinic.ports.doctor.model.Doctor;
import com.jaffy.clinic.ports.doctor.repository.IDoctorRepository;
import com.jaffy.clinic.ports.patient.model.Patient;
import com.jaffy.clinic.ports.patient.repository.IPatientRepository;
import com.jaffy.clinic.ports.user.model.User;
import com.jaffy.clinic.ports.user.repository.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AppointmentService implements IAppointmentService {

    private final IAppointmentRepository appointmentRepository;
    private final IUserRepository userRepository;
    private final IPatientRepository patientRepository;
    private final IDoctorRepository doctorRepository;

    @Override
    @Transactional
    public void addAppointment(Appointment appointment, String patientUsername, String doctorUsername) throws ValidationFailedException, NotFoundException {
        Patient patient = patientRepository.findByUsername(patientUsername)
                .orElseThrow(() -> new NotFoundException(Patient.class, String.format("username %s", patientUsername)));
        appointment.setPatient(patient);

        Doctor doctor = doctorRepository.findByUsername(doctorUsername)
                .orElseThrow(() -> new NotFoundException(Doctor.class, String.format("username %s", doctorUsername)));
        appointment.setDoctor(doctor);

        List<Appointment> saveAppointments = appointmentRepository.findAllByPatientAndDoctor(appointment.getPatient(), appointment.getDoctor());
        for (Appointment saveAppointmentDto : saveAppointments) {
            if (appointment.getStartTimeVisit().isEqual(saveAppointmentDto.getEndTimeVisit())) {
                throw new ValidationFailedException("Invalid start time visit");
            } else if (appointment.getStartTimeVisit().isBefore(saveAppointmentDto.getEndTimeVisit())) {
                if (saveAppointmentDto.getStartTimeVisit().isAfter(appointment.getStartTimeVisit()) || appointment.getEndTimeVisit().isAfter(saveAppointmentDto.getStartTimeVisit())) {
                    throw new ValidationFailedException("Invalid start time visit");
                }
            }
        }

        appointmentRepository.save(appointment);
    }

    @Override
    public List<Appointment> getAllAppointmentByUsername(String username, Pageable pageable) throws ValidationFailedException, NotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new NotFoundException(User.class, String.format("username %s", username)));

        return appointmentRepository.findAllByUser(user, pageable);
    }

    @Override
    @Transactional
    public void deleteAppointment(Integer id) throws NotFoundException {
        Appointment appointment = appointmentRepository.findById(id);
        appointmentRepository.deleteAppointment(appointment);
    }
}
