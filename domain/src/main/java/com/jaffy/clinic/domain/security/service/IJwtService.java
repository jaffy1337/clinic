package com.jaffy.clinic.domain.security.service;

import org.springframework.security.core.Authentication;

public interface IJwtService {
    String generateToken(Authentication authentication);

    String getUsernameFromToken(String token);

    boolean validateToken(String authToken);
}
