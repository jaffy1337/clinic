package com.jaffy.clinic.domain.security.service.impl;


import com.jaffy.clinic.domain.security.config.AppSecurityProperties;
import com.jaffy.clinic.domain.security.service.IJwtService;
import com.jaffy.clinic.ports.user.model.User;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Date;

@Slf4j
@Service
@RequiredArgsConstructor
public class JwtService implements IJwtService {

    private final AppSecurityProperties appSecurityProperties;

    private static Date toDate(ZonedDateTime dateTime) {
        return Date.from(dateTime.toInstant());
    }

    @Override
    public String generateToken(Authentication authentication) {
        final User userPrincipal = (User) authentication.getPrincipal();
        final ZonedDateTime now = ZonedDateTime.now();

        return Jwts.builder()
                .setSubject(userPrincipal.getUsername())
                .setIssuedAt(toDate(now))
                .setExpiration(toDate(now.plusSeconds(appSecurityProperties.getJwtExpirationTime() / 1000)))
                .signWith(SignatureAlgorithm.HS512, appSecurityProperties.getJwtSecret())
                .compact();
    }

    @Override
    public String getUsernameFromToken(String token) {
        return Jwts.parser().setSigningKey(appSecurityProperties.getJwtSecret()).parseClaimsJws(token).getBody().getSubject();
    }

    @Override
    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(appSecurityProperties.getJwtSecret()).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            log.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            log.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }
}
