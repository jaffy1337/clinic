package com.jaffy.clinic.domain.user.service;

import com.jaffy.clinic.domain.security.service.IJwtService;
import com.jaffy.clinic.ports.common.exception.AlreadyExistsException;
import com.jaffy.clinic.ports.user.model.User;
import com.jaffy.clinic.ports.user.repository.IUserRepository;
import com.jaffy.clinic.ports.user.service.IAuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class AuthService implements IAuthService {

    private final AuthenticationManager authenticationManager;
    private final IJwtService jwtService;
    private final IUserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public String loginUser(String username, String password) {
        final Authentication authentication = this.authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return this.jwtService.generateToken(authentication);
    }

    @Override
    @Transactional
    public void registerUser(User user) throws AlreadyExistsException {
        if (userRepository.validIsUserExist(user.getUsername(), user.getEmail())) {
            throw new AlreadyExistsException(User.class, String.format("username %s and email %s", user.getUsername(), user.getEmail()));
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }
}
