package com.jaffy.clinic.ports.patient.repository;

import com.jaffy.clinic.ports.patient.model.Patient;

import java.util.Optional;

public interface IPatientRepository {

    Optional<Patient> findByUsername(String username);
}
