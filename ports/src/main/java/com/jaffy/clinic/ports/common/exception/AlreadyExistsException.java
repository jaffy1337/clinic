package com.jaffy.clinic.ports.common.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class AlreadyExistsException extends DomainException {
    private final Class<?> existingClass;
    private final String existingValue;
}
