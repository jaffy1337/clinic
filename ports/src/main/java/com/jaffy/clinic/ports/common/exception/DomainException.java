package com.jaffy.clinic.ports.common.exception;

public abstract class DomainException extends Exception {

    protected DomainException() {

    }

    protected DomainException(String message) {
        super(message);
    }

    protected DomainException(Throwable cause) {
        super(cause);
    }
}
