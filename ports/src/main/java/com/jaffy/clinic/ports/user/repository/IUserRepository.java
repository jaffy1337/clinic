package com.jaffy.clinic.ports.user.repository;

import com.jaffy.clinic.ports.user.model.User;

import java.util.Optional;

public interface IUserRepository {
    Optional<User> findByUsername(String username);

    boolean validIsUserExist(String username, String email);

    User save(User user);
}
