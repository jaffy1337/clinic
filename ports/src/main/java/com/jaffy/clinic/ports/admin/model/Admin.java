package com.jaffy.clinic.ports.admin.model;

import com.jaffy.clinic.ports.user.model.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class Admin extends User {
}
