package com.jaffy.clinic.ports.appointment.service;

import com.jaffy.clinic.ports.appointment.model.Appointment;
import com.jaffy.clinic.ports.common.exception.NotFoundException;
import com.jaffy.clinic.ports.common.exception.ValidationFailedException;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IAppointmentService {

    void addAppointment(Appointment appointment, String patientUsername, String doctorUsername) throws ValidationFailedException, NotFoundException;

    List<Appointment> getAllAppointmentByUsername(String username, Pageable pageable) throws ValidationFailedException, NotFoundException;

    void deleteAppointment(Integer id) throws NotFoundException;
}
