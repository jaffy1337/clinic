package com.jaffy.clinic.ports.user.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
public class Address implements Serializable {
    private Integer id;
    private String country;
    private String city;
    private String postalCode;
    private String street;
}
