package com.jaffy.clinic.ports.appointment.model;

import com.jaffy.clinic.ports.doctor.model.Doctor;
import com.jaffy.clinic.ports.patient.model.Patient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
@AllArgsConstructor
public class Appointment {
    private Integer id;
    private Patient patient;
    private Doctor doctor;
    private ZonedDateTime startTimeVisit;
    private ZonedDateTime endTimeVisit;
    private String patientNote;
}
