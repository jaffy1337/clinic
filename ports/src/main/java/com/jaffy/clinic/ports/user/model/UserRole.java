package com.jaffy.clinic.ports.user.model;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

@Getter
public enum UserRole implements GrantedAuthority {
    DOCTOR(Constants.DOCTOR_VALUE),
    ADMIN(Constants.ADMIN_VALUE),
    PATIENT(Constants.PATIENT_VALUE);

    private final String value;

    UserRole(String roleValue) {
        this.value = roleValue;
    }

    @Override
    public String getAuthority() {
        return "ROLE_" + value;
    }

    public static class Constants {
        public static final String DOCTOR_VALUE = "DOCTOR";
        public static final String ADMIN_VALUE = "ADMIN";
        public static final String PATIENT_VALUE = "PATIENT";

        private Constants() {
        }
    }
}