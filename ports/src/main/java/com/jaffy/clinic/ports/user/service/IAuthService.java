package com.jaffy.clinic.ports.user.service;

import com.jaffy.clinic.ports.common.exception.AlreadyExistsException;
import com.jaffy.clinic.ports.user.model.User;

public interface IAuthService {

    String loginUser(String username, String password);

    void registerUser(User register) throws AlreadyExistsException;
}
