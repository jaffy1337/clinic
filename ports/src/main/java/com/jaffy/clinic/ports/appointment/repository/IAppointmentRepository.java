package com.jaffy.clinic.ports.appointment.repository;

import com.jaffy.clinic.ports.appointment.model.Appointment;
import com.jaffy.clinic.ports.common.exception.NotFoundException;
import com.jaffy.clinic.ports.common.exception.ValidationFailedException;
import com.jaffy.clinic.ports.doctor.model.Doctor;
import com.jaffy.clinic.ports.patient.model.Patient;
import com.jaffy.clinic.ports.user.model.User;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IAppointmentRepository {

    Appointment save(Appointment user);

    List<Appointment> findAllByPatientAndDoctor(Patient patient, Doctor doctor);

    List<Appointment> findAllByUser(User user, Pageable pageable) throws NotFoundException, ValidationFailedException;

    Appointment findById(Integer id) throws NotFoundException;

    void deleteAppointment(Appointment appointment);
}
