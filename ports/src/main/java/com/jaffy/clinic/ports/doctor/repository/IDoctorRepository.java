package com.jaffy.clinic.ports.doctor.repository;

import com.jaffy.clinic.ports.doctor.model.Doctor;

import java.util.Optional;

public interface IDoctorRepository {

    Optional<Doctor> findByUsername(String username);
}
