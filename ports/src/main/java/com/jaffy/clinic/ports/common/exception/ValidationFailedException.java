package com.jaffy.clinic.ports.common.exception;

public class ValidationFailedException extends DomainException {
    public ValidationFailedException(String message) {
        super(message);
    }
}