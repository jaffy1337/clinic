package com.jaffy.clinic.ports.common.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class NotFoundException extends DomainException {
    private final Class<?> notFoundClass;
    private final String notFoundValue;
}
