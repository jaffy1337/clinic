package com.jaffy.clinic.ports.user.model;

public enum Gender {
    MALE,
    FEMALE,
    UNKNOWN
}
