package com.jaffy.clinic.adapter.rest.common.handler;

import com.jaffy.clinic.ports.common.exception.AlreadyExistsException;
import com.jaffy.clinic.ports.common.exception.DomainException;
import com.jaffy.clinic.ports.common.exception.NotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class DomainExceptionHandler {

    @ExceptionHandler(AlreadyExistsException.class)
    ResponseEntity<String> alreadyExistsException(AlreadyExistsException ex) {
        var message = String.format("%s with %s already exists", ex.getExistingClass().getSimpleName(), ex.getExistingValue());
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    ResponseEntity<String> notFoundException(NotFoundException ex) {
        var message = String.format("%s with %s does not exist", ex.getNotFoundClass().getSimpleName(), ex.getNotFoundValue());
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DomainException.class)
    ResponseEntity<String> domainException(DomainException ex) {
        return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
    }
}
