package com.jaffy.clinic.adapter.rest.user;

import com.jaffy.clinic.adapter.rest.user.model.RegisterCommand;
import com.jaffy.clinic.ports.common.exception.AlreadyExistsException;
import com.jaffy.clinic.ports.user.service.IAuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "User management controller")
@RestController
@RequiredArgsConstructor
@RequestMapping(UserController.USER_API)
public class UserController {
    static final String USER_API = "/api/users";

    private final IAuthService userService;

    @Operation(summary = "Request login")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Request login accepted"),
            @ApiResponse(responseCode = "400", description = "Bad Request, validation failed"),
            @ApiResponse(responseCode = "500", description = "Server error, something went wrong")
    })
    @PostMapping("/login")
    ResponseEntity<String> login(@RequestParam("username") final String username,
                                 @RequestParam("password") final String password) {
        String token = userService.loginUser(username, password);
        return new ResponseEntity<>(token, HttpStatus.OK);
    }

    @Operation(summary = "Request register")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Request register accepted"),
            @ApiResponse(responseCode = "400", description = "Bad Request, validation failed"),
            @ApiResponse(responseCode = "500", description = "Server error, something went wrong")
    })
    @PostMapping("/register")
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    void register(@RequestBody @Valid RegisterCommand register) throws AlreadyExistsException {
        userService.registerUser(register.toDomain());
    }
}
