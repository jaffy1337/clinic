package com.jaffy.clinic.adapter.rest.user.validator;

import org.springframework.beans.factory.annotation.Configurable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

@Configurable
public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {

    private static final Pattern VALID_PHONE_REGEX =
            Pattern.compile("^([+]?[\\s0-9]+)?(\\d{3}|[(]?[0-9]+[)])?([-]?[\\s]?[0-9])+$");

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // The Lookup API requires your phone number in E.164 format
        // E.164 formatted phone numbers must not have spaces in them
        value = value.replaceAll("[\\s()-]", "");

        if ("".equals(value)) {
            return false;
        }

        return Pattern.matches(VALID_PHONE_REGEX.pattern(), value);
    }
}