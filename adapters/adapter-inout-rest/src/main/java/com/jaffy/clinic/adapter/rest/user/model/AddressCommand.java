package com.jaffy.clinic.adapter.rest.user.model;

import com.jaffy.clinic.ports.user.model.Address;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;

public record AddressCommand(
        @Schema(description = "country",
                example = "Poland", required = true)
        @NotBlank
        String country,

        @Schema(description = "city",
                example = "Warsaw", required = true)
        @NotBlank
        String city,

        @Schema(description = "Street name with house number",
                example = "Al. Solidarności 12/23", required = true)
        @NotBlank
        String street,

        @Schema(description = "Postal code",
                example = "01-235", required = true)
        @NotBlank
        String postalCode
) {

    public Address toDomain() {
        return Address.builder()
                .city(city)
                .country(country)
                .postalCode(postalCode)
                .street(street)
                .build();
    }
}
