package com.jaffy.clinic.adapter.rest.user.validator;

import org.springframework.beans.factory.annotation.Configurable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

@Configurable
public class PasswordValidator implements ConstraintValidator<Password, String> {

    private static final Pattern VALID_PASSWORD_REGEX =
            Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$", Pattern.CASE_INSENSITIVE);

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if ("".equals(value)) {
            return false;
        }

        return Pattern.matches(VALID_PASSWORD_REGEX.pattern(), value);
    }
}