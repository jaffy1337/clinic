package com.jaffy.clinic.adapter.rest.appointment.model;

import com.jaffy.clinic.ports.appointment.model.Appointment;
import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

public record AppointmentCommand(
        @Schema(description = "Patient username",
                example = "patient", required = true)
        @NotEmpty String patientUsername,

        @Schema(description = "Doctor username",
                example = "doctor", required = true)
        @NotEmpty String doctorUsername,

        @Schema(description = "Start time visit", required = true)
        @NotNull @Future ZonedDateTime startTimeVisit,

        @Schema(description = "Time duration of visit",
                example = "15", required = true)
        @NotNull @Range(min = 15) Integer durationOfVisit,

        @Schema(description = "Patient note", example = "Patient note")
        String patientNote
) {
    public Appointment toDomain() {
        return Appointment.builder()
                .patientNote(patientNote)
                .startTimeVisit(startTimeVisit)
                .endTimeVisit(startTimeVisit.plusMinutes(durationOfVisit))
                .build();
    }
}
