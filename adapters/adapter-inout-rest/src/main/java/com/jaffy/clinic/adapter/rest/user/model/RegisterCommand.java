package com.jaffy.clinic.adapter.rest.user.model;

import com.jaffy.clinic.adapter.rest.user.validator.Password;
import com.jaffy.clinic.adapter.rest.user.validator.PhoneNumber;
import com.jaffy.clinic.ports.user.model.Gender;
import com.jaffy.clinic.ports.user.model.User;
import com.jaffy.clinic.ports.user.model.UserRole;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;

public record RegisterCommand(
        @Schema(description = "username",
                example = "user", required = true)
        @NotBlank @Size(min = 3, max = 256)
        String username,

        @Schema(description = "password must least eight characters," +
                " one lowercase character (a-z), one uppercase character (A-Z)," +
                " one digit (0-9), one symbol (? . , ! _ - ~ $ % + =)",
                example = "Password1@", required = true)
        @NotBlank @Size(min = 8, max = 256) @Password
        String password,

        @Schema(description = "first name",
                example = "User", required = true)
        @NotBlank @Size(min = 3, max = 256)
        String name,

        @Schema(description = "last name",
                example = "User", required = true)
        @NotBlank @Size(min = 3, max = 256)
        String lastName,

        @Schema(description = "email",
                example = "user@example.com", required = true)
        @NotBlank @Email
        String email,

        @Schema(description = "phone number",
                example = "+48 529 444 333", required = true)
        @NotBlank @PhoneNumber
        String phone,

        @Schema(description = "role account",
                example = "PATIENT", required = true)
        @NotNull
        UserRole role,

        @Schema(description = "gender",
                example = "MALE", required = true)
        @NotNull
        Gender gender,

        @Schema(description = "date of birth",
                example = "2020-01-02", required = true)
        @NotNull @Past
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        LocalDate dateBirth,

        @Schema(description = "address", required = true)
        @NotNull
        AddressCommand address
) {

    public User toDomain() {
        return User.builder()
                .username(username)
                .password(password)
                .role(role)
                .name(name)
                .lastName(lastName)
                .email(email)
                .phone(phone)
                .gender(gender)
                .dateBirth(dateBirth)
                .address(address.toDomain())
                .build();
    }
}
