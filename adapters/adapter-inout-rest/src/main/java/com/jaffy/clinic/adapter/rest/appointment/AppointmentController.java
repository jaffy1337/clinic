package com.jaffy.clinic.adapter.rest.appointment;

import com.jaffy.clinic.adapter.rest.appointment.model.AppointmentCommand;
import com.jaffy.clinic.adapter.rest.appointment.model.AppointmentResponse;
import com.jaffy.clinic.ports.appointment.service.IAppointmentService;
import com.jaffy.clinic.ports.common.exception.NotFoundException;
import com.jaffy.clinic.ports.common.exception.ValidationFailedException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "Appointment management controller")
@RestController
@RequiredArgsConstructor
@RequestMapping(AppointmentController.APPOINTMENT_API)
public class AppointmentController {
    static final String APPOINTMENT_API = "/api/appointment";

    private final IAppointmentService appointmentService;

    @Secured({"ROLE_ADMIN"})
    @Operation(summary = "Request add appointment", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Appointment created"),
            @ApiResponse(responseCode = "400", description = "Bad Request, validation failed"),
            @ApiResponse(responseCode = "500", description = "Server error, something went wrong")
    })
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    void addAppointment(@RequestBody @Valid AppointmentCommand appointment) throws NotFoundException, ValidationFailedException {
        appointmentService.addAppointment(appointment.toDomain(), appointment.patientUsername(), appointment.doctorUsername());
    }

    @Secured({"ROLE_ADMIN"})
    @Operation(summary = "Request delete appointment", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Appointment deleted"),
            @ApiResponse(responseCode = "400", description = "Bad Request, validation failed"),
            @ApiResponse(responseCode = "500", description = "Server error, something went wrong")
    })
    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    void deleteAppointment(@PathVariable("id") final Integer id) throws NotFoundException {
        appointmentService.deleteAppointment(id);
    }

    @Secured({"ROLE_ADMIN", "ROLE_PATIENT", "ROLE_DOCTOR"})
    @Operation(summary = "Request get all appointments by username", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Request accepted"),
            @ApiResponse(responseCode = "400", description = "Bad Request, validation failed"),
            @ApiResponse(responseCode = "500", description = "Server error, something went wrong")
    })
    @GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    List<AppointmentResponse> findAllAppointmentByUsername(
            @RequestParam("username") final String username, @Parameter(hidden = true) @PageableDefault(sort = "id") Pageable pageable)
            throws NotFoundException, ValidationFailedException {
        return appointmentService.getAllAppointmentByUsername(username, pageable).stream()
                .map(AppointmentResponse::from)
                .toList();
    }
}
