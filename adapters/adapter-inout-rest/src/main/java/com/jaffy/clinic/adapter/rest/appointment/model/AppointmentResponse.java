package com.jaffy.clinic.adapter.rest.appointment.model;

import com.jaffy.clinic.ports.appointment.model.Appointment;

import java.time.ZonedDateTime;

public record AppointmentResponse(
        Integer id,
        String patientUsername,
        String doctorUsername,
        ZonedDateTime startTimeVisit,
        ZonedDateTime endTimeVisit,
        String patientNote
) {
    public static AppointmentResponse from(Appointment appointment) {
        return new AppointmentResponse(
                appointment.getId(),
                appointment.getPatient().getUsername(),
                appointment.getDoctor().getUsername(),
                appointment.getStartTimeVisit(),
                appointment.getEndTimeVisit(),
                appointment.getPatientNote()
        );
    }
}
