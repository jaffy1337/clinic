package com.jaffy.clinic.adapter.db.patient.repository.impl;

import com.jaffy.clinic.adapter.db.patient.model.PatientEntity;
import com.jaffy.clinic.adapter.db.patient.repository.ISpringPatientRepository;
import com.jaffy.clinic.ports.patient.model.Patient;
import com.jaffy.clinic.ports.patient.repository.IPatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class PatientRepository implements IPatientRepository {

    private final ISpringPatientRepository springPatientRepository;

    @Override
    public Optional<Patient> findByUsername(String username) {
        return springPatientRepository.findByUsername(username)
                .map(PatientEntity::toDomain);
    }
}
