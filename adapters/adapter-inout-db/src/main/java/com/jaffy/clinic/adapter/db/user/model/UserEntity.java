package com.jaffy.clinic.adapter.db.user.model;

import com.jaffy.clinic.ports.user.model.Gender;
import com.jaffy.clinic.ports.user.model.User;
import com.jaffy.clinic.ports.user.model.UserRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Entity(name = "users")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "use_role",
        discriminatorType = DiscriminatorType.STRING
)
public abstract class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "use_id")
    private Integer id;

    @Column(name = "use_username", unique = true)
    private String username;

    @Column(name = "use_password")
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_role", updatable = false, insertable = false)
    private UserRole role;

    @Column(name = "use_name")
    private String name;

    @Column(name = "use_last_name")
    private String lastName;

    @Column(name = "use_email", unique = true)
    private String email;

    @Column(name = "use_phone", unique = true)
    private String phone;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_gender")
    private Gender gender;

    @Column(name = "use_date_birth")
    private LocalDate dateBirth;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "use_add_id", referencedColumnName = "add_id")
    private AddressEntity address;

    @UpdateTimestamp
    @Column(name = "use_audit_md")
    private ZonedDateTime modificationDay;

    @CreationTimestamp
    @Column(name = "use_audit_cd", updatable = false)
    private ZonedDateTime createdDay;

    public abstract User toDomain();
}
