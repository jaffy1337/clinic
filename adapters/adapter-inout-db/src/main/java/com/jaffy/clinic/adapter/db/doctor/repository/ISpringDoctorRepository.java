package com.jaffy.clinic.adapter.db.doctor.repository;

import com.jaffy.clinic.adapter.db.doctor.model.DoctorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ISpringDoctorRepository extends JpaRepository<DoctorEntity, Integer> {

    Optional<DoctorEntity> findByUsername(String username);
}
