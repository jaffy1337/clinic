package com.jaffy.clinic.adapter.db.user.repository.impl;

import com.jaffy.clinic.adapter.db.admin.model.AdminEntity;
import com.jaffy.clinic.adapter.db.doctor.model.DoctorEntity;
import com.jaffy.clinic.adapter.db.patient.model.PatientEntity;
import com.jaffy.clinic.adapter.db.user.model.UserEntity;
import com.jaffy.clinic.adapter.db.user.repository.ISpringUserRepository;
import com.jaffy.clinic.ports.user.model.User;
import com.jaffy.clinic.ports.user.repository.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class UserRepository implements IUserRepository {

    private final ISpringUserRepository springUserRepository;

    @Override
    public Optional<User> findByUsername(String username) {
        return springUserRepository
                .findByUsername(username)
                .map(UserEntity::toDomain);
    }

    @Override
    public boolean validIsUserExist(String username, String email) {
        return springUserRepository.existsByUsernameOrEmail(username, email);
    }

    @Override
    public User save(User user) {
        UserEntity userEntity = switch (user.getRole()) {
            case ADMIN -> springUserRepository.save(AdminEntity.from(user));
            case DOCTOR -> springUserRepository.save(DoctorEntity.from(user));
            case PATIENT -> springUserRepository.save(PatientEntity.from(user));
        };

        return userEntity.toDomain();
    }
}
