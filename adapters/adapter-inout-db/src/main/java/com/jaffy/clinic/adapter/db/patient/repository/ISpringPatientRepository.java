package com.jaffy.clinic.adapter.db.patient.repository;

import com.jaffy.clinic.adapter.db.patient.model.PatientEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ISpringPatientRepository extends JpaRepository<PatientEntity, Integer> {

    Optional<PatientEntity> findByUsername(String username);
}
