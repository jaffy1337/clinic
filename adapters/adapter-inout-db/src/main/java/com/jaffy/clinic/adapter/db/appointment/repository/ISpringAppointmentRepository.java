package com.jaffy.clinic.adapter.db.appointment.repository;

import com.jaffy.clinic.adapter.db.appointment.model.AppointmentEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ISpringAppointmentRepository extends PagingAndSortingRepository<AppointmentEntity, Integer> {

    List<AppointmentEntity> findAllByPatient_IdAndDoctor_Id(Integer patientId, Integer doctorId);

    List<AppointmentEntity> findAllByPatient_Id(Integer patientId, Pageable pageable);

    List<AppointmentEntity> findAllByDoctor_Id(Integer doctorId, Pageable pageable);
}
