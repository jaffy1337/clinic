package com.jaffy.clinic.adapter.db.admin.model;

import com.jaffy.clinic.adapter.db.user.model.AddressEntity;
import com.jaffy.clinic.adapter.db.user.model.UserEntity;
import com.jaffy.clinic.ports.admin.model.Admin;
import com.jaffy.clinic.ports.user.model.User;
import com.jaffy.clinic.ports.user.model.UserRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@DiscriminatorValue(value = UserRole.Constants.ADMIN_VALUE)
public class AdminEntity extends UserEntity {

    public static AdminEntity from(User user) {
        return AdminEntity.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .name(user.getName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .phone(user.getPhone())
                .gender(user.getGender())
                .dateBirth(user.getDateBirth())
                .address(AddressEntity.from(user.getAddress()))
                .build();
    }

    @Override
    public Admin toDomain() {
        return Admin.builder()
                .id(getId())
                .username(getUsername())
                .password(getPassword())
                .role(getRole())
                .name(getName())
                .lastName(getLastName())
                .email(getEmail())
                .phone(getPhone())
                .gender(getGender())
                .dateBirth(getDateBirth())
                .address(getAddress().toDomain())
                .build();
    }
}
