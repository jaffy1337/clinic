package com.jaffy.clinic.adapter.db.patient.model;

import com.jaffy.clinic.adapter.db.user.model.AddressEntity;
import com.jaffy.clinic.adapter.db.user.model.UserEntity;
import com.jaffy.clinic.ports.patient.model.Patient;
import com.jaffy.clinic.ports.user.model.User;
import com.jaffy.clinic.ports.user.model.UserRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@DiscriminatorValue(value = UserRole.Constants.PATIENT_VALUE)
public class PatientEntity extends UserEntity {

    public static PatientEntity from(User user) {
        return PatientEntity.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .name(user.getName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .phone(user.getPhone())
                .gender(user.getGender())
                .dateBirth(user.getDateBirth())
                .address(AddressEntity.from(user.getAddress()))
                .build();
    }

    public static PatientEntity from(Patient patient) {
        return PatientEntity.builder()
                .id(patient.getId())
                .username(patient.getUsername())
                .password(patient.getPassword())
                .name(patient.getName())
                .lastName(patient.getLastName())
                .email(patient.getEmail())
                .phone(patient.getPhone())
                .gender(patient.getGender())
                .dateBirth(patient.getDateBirth())
                .address(AddressEntity.from(patient.getAddress()))
                .build();
    }

    @Override
    public Patient toDomain() {
        return Patient.builder()
                .id(getId())
                .username(getUsername())
                .password(getPassword())
                .role(getRole())
                .name(getName())
                .lastName(getLastName())
                .email(getEmail())
                .phone(getPhone())
                .gender(getGender())
                .dateBirth(getDateBirth())
                .address(getAddress().toDomain())
                .build();
    }
}
