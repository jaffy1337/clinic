package com.jaffy.clinic.adapter.db.admin.repository;

import com.jaffy.clinic.adapter.db.user.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISpringAdminRepository extends JpaRepository<UserEntity, Integer> {
}
