package com.jaffy.clinic.adapter.db.doctor.model;

import com.jaffy.clinic.adapter.db.user.model.AddressEntity;
import com.jaffy.clinic.adapter.db.user.model.UserEntity;
import com.jaffy.clinic.ports.doctor.model.Doctor;
import com.jaffy.clinic.ports.user.model.User;
import com.jaffy.clinic.ports.user.model.UserRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@DiscriminatorValue(value = UserRole.Constants.DOCTOR_VALUE)
public class DoctorEntity extends UserEntity {

    public static DoctorEntity from(User user) {
        return DoctorEntity.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .name(user.getName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .phone(user.getPhone())
                .gender(user.getGender())
                .dateBirth(user.getDateBirth())
                .address(AddressEntity.from(user.getAddress()))
                .build();
    }

    public static DoctorEntity from(Doctor doctor) {
        return DoctorEntity.builder()
                .id(doctor.getId())
                .username(doctor.getUsername())
                .password(doctor.getPassword())
                .name(doctor.getName())
                .lastName(doctor.getLastName())
                .email(doctor.getEmail())
                .phone(doctor.getPhone())
                .gender(doctor.getGender())
                .dateBirth(doctor.getDateBirth())
                .address(AddressEntity.from(doctor.getAddress()))
                .build();
    }

    @Override
    public Doctor toDomain() {
        return Doctor.builder()
                .id(getId())
                .username(getUsername())
                .password(getPassword())
                .role(getRole())
                .name(getName())
                .lastName(getLastName())
                .email(getEmail())
                .phone(getPhone())
                .gender(getGender())
                .dateBirth(getDateBirth())
                .address(getAddress().toDomain())
                .build();
    }
}
