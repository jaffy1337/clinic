package com.jaffy.clinic.adapter.db.appointment.model;

import com.jaffy.clinic.adapter.db.doctor.model.DoctorEntity;
import com.jaffy.clinic.adapter.db.patient.model.PatientEntity;
import com.jaffy.clinic.ports.appointment.model.Appointment;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "appointments")
public class AppointmentEntity {

    @Id
    @Column(name = "app_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "app_use_id_patient")
    private PatientEntity patient;

    @ManyToOne
    @JoinColumn(name = "app_use_id_doctor")
    private DoctorEntity doctor;

    @Column(name = "app_start_time_visit")
    private ZonedDateTime startTimeVisit;

    @Column(name = "app_end_time_visit")
    private ZonedDateTime endTimeVisit;

    @Column(name = "app_patient_note")
    private String patientNote;

    @UpdateTimestamp
    @Column(name = "app_audit_md")
    private ZonedDateTime modificationDay;

    @CreationTimestamp
    @Column(name = "app_audit_cd", updatable = false)
    private ZonedDateTime createdDay;

    public static AppointmentEntity from(Appointment appointment) {
        return AppointmentEntity.builder()
                .id(appointment.getId())
                .patient(PatientEntity.from(appointment.getPatient()))
                .doctor(DoctorEntity.from(appointment.getDoctor()))
                .startTimeVisit(appointment.getStartTimeVisit())
                .endTimeVisit(appointment.getEndTimeVisit())
                .patientNote(appointment.getPatientNote())
                .build();
    }

    public Appointment toDomain() {
        return Appointment.builder()
                .id(id)
                .startTimeVisit(startTimeVisit)
                .endTimeVisit(endTimeVisit)
                .doctor(doctor.toDomain())
                .patient(patient.toDomain())
                .patientNote(patientNote)
                .build();
    }
}
