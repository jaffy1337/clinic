package com.jaffy.clinic.adapter.db.doctor.repository.impl;

import com.jaffy.clinic.adapter.db.doctor.model.DoctorEntity;
import com.jaffy.clinic.adapter.db.doctor.repository.ISpringDoctorRepository;
import com.jaffy.clinic.ports.doctor.model.Doctor;
import com.jaffy.clinic.ports.doctor.repository.IDoctorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class DoctorRepository implements IDoctorRepository {

    private final ISpringDoctorRepository springDoctorRepository;

    @Override
    public Optional<Doctor> findByUsername(String username) {
        return springDoctorRepository.findByUsername(username)
                .map(DoctorEntity::toDomain);
    }
}
