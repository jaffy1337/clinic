package com.jaffy.clinic.adapter.db.user.model;

import com.jaffy.clinic.ports.user.model.Address;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "addresses")
public class AddressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "add_id")
    private Integer id;

    @Column(name = "add_country")
    private String country;

    @Column(name = "add_city")
    private String city;

    @Column(name = "add_postal_code")
    private String postalCode;

    @Column(name = "add_street")
    private String street;

    @UpdateTimestamp
    @Column(name = "add_audit_md")
    private ZonedDateTime modificationDay;

    @CreationTimestamp
    @Column(name = "add_audit_cd", updatable = false)
    private ZonedDateTime createdDay;

    public static AddressEntity from(Address address) {
        return AddressEntity.builder()
                .id(address.getId())
                .city(address.getCity())
                .country(address.getCountry())
                .postalCode(address.getPostalCode())
                .street(address.getStreet())
                .build();
    }

    public Address toDomain() {
        return Address.builder()
                .id(id)
                .city(city)
                .country(country)
                .postalCode(postalCode)
                .street(street)
                .build();
    }
}
