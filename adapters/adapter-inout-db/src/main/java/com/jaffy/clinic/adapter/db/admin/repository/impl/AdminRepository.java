package com.jaffy.clinic.adapter.db.admin.repository.impl;

import com.jaffy.clinic.ports.admin.repository.IAdminRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class AdminRepository implements IAdminRepository {
}
