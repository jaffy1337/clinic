package com.jaffy.clinic.adapter.db.appointment.repository.impl;

import com.jaffy.clinic.adapter.db.appointment.model.AppointmentEntity;
import com.jaffy.clinic.adapter.db.appointment.repository.ISpringAppointmentRepository;
import com.jaffy.clinic.ports.appointment.model.Appointment;
import com.jaffy.clinic.ports.appointment.repository.IAppointmentRepository;
import com.jaffy.clinic.ports.common.exception.NotFoundException;
import com.jaffy.clinic.ports.common.exception.ValidationFailedException;
import com.jaffy.clinic.ports.doctor.model.Doctor;
import com.jaffy.clinic.ports.patient.model.Patient;
import com.jaffy.clinic.ports.user.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class AppointmentRepository implements IAppointmentRepository {

    private final ISpringAppointmentRepository springAppointmentRepository;

    @Override
    public Appointment save(Appointment appointment) {
        AppointmentEntity appointmentEntity = AppointmentEntity.from(appointment);
        return springAppointmentRepository.save(appointmentEntity).toDomain();
    }

    @Override
    public List<Appointment> findAllByPatientAndDoctor(Patient patient, Doctor doctor) {
        return springAppointmentRepository.findAllByPatient_IdAndDoctor_Id(patient.getId(), doctor.getId())
                .stream()
                .map(AppointmentEntity::toDomain)
                .toList();
    }

    @Override
    public List<Appointment> findAllByUser(User user, Pageable pageable) throws ValidationFailedException {
        List<AppointmentEntity> list = switch (user.getRole()) {
            case DOCTOR -> springAppointmentRepository.findAllByDoctor_Id(user.getId(), pageable);
            case PATIENT -> springAppointmentRepository.findAllByPatient_Id(user.getId(), pageable);
            default -> throw new ValidationFailedException(String.format("Invalid username %s", user.getUsername()));
        };

        return list.stream()
                .map(AppointmentEntity::toDomain)
                .toList();
    }

    @Override
    public Appointment findById(Integer id) throws NotFoundException {
        return springAppointmentRepository.findById(id)
                .map(AppointmentEntity::toDomain)
                .orElseThrow(() -> new NotFoundException(Patient.class, String.format("id %s", id)));
    }

    @Override
    public void deleteAppointment(Appointment appointment) {
        springAppointmentRepository.delete(AppointmentEntity.from(appointment));
    }
}
