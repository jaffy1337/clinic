CREATE TABLE users
(
    use_id         SERIAL                              NOT NULL,
    use_username   VARCHAR                             NOT NULL,
    use_password   VARCHAR                             NOT NULL,
    use_role       VARCHAR                             NOT NULL,
    use_name       VARCHAR                             NOT NULL,
    use_last_name  VARCHAR                             NOT NULL,
    use_email      VARCHAR                             NOT NULL,
    use_phone      VARCHAR                             NULL,
    use_gender     VARCHAR                             NOT NULL,
    use_date_birth DATE                                NOT NULL,
    use_add_id     SERIAL                              NOT NULL,
    use_audit_cd   TIMESTAMP DEFAULT current_timestamp NOT NULL,
    use_audit_md   TIMESTAMP                           NULL
);

ALTER TABLE users
    ADD CONSTRAINT pk_user_accounts PRIMARY KEY (use_id);

ALTER TABLE users
    ADD CONSTRAINT fk_users_addresses
        FOREIGN KEY (use_add_id)
            REFERENCES addresses (add_id);