CREATE TABLE appointments
(
    app_id               SERIAL                              NOT NULL,
    app_use_id_patient   SERIAL                              NOT NULL,
    app_use_id_doctor    SERIAL                              NOT NULL,
    app_start_time_visit TIMESTAMP                           NOT NULL,
    app_end_time_visit   TIMESTAMP                           NOT NULL,
    app_patient_note     VARCHAR                             NULL,
    app_audit_cd         TIMESTAMP DEFAULT current_timestamp NOT NULL,
    app_audit_md         TIMESTAMP                           NULL
);

ALTER TABLE appointments
    ADD CONSTRAINT pk_appointments PRIMARY KEY (app_id);

ALTER TABLE appointments
    ADD CONSTRAINT fk_appointments_patients_user
        FOREIGN KEY (app_use_id_patient)
            REFERENCES users (use_id);

ALTER TABLE appointments
    ADD CONSTRAINT fk_appointments_doctors_user
        FOREIGN KEY (app_use_id_doctor)
            REFERENCES users (use_id);
