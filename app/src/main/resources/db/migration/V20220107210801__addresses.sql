CREATE TABLE addresses
(
    add_id          SERIAL                              NOT NULL,
    add_country     VARCHAR                             NOT NULL,
    add_city        VARCHAR                             NOT NULL,
    add_postal_code VARCHAR                             NOT NULL,
    add_street      VARCHAR                             NOT NULL,
    add_audit_cd    TIMESTAMP DEFAULT current_timestamp NOT NULL,
    add_audit_md    TIMESTAMP NULL
);

ALTER TABLE addresses
    ADD CONSTRAINT pk_addresses PRIMARY KEY (add_id);