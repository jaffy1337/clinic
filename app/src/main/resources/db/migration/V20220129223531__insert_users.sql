insert into addresses (add_city, add_country, add_street, add_postal_code, add_audit_cd, add_audit_md)
values ('Warsaw', 'Poland', 'Al. Solidarności 12/23', '01-235', '2022-01-29 22:35:50.203676',
        '2022-01-29 22:35:50.203676');

insert into users (use_add_id, use_date_birth, use_email, use_gender, use_last_name, use_name, use_password, use_phone,
                   use_username, use_role, use_audit_cd, use_audit_md)
values (1, '2016-01-02', 'patient@example.com', 'MALE', 'patient', 'patient',
        '$2a$10$iN1NlTwea59/dYa1ubrd.uzam6XxAdMWNgM2.66JbZTx4.5GmM9he', '+48 529 444 333', 'patient', 'PATIENT',
        current_timestamp, current_timestamp);
insert into users (use_add_id, use_date_birth, use_email, use_gender, use_last_name, use_name, use_password, use_phone,
                   use_username, use_role, use_audit_cd, use_audit_md)
values (1, '2020-01-02', 'doctor@example.com', 'FEMALE', 'doctor', 'doctor',
        '$2a$10$iN1NlTwea59/dYa1ubrd.uzam6XxAdMWNgM2.66JbZTx4.5GmM9he', '+48 529 444 333', 'doctor', 'DOCTOR',
        current_timestamp, current_timestamp);
insert into users (use_add_id, use_date_birth, use_email, use_gender, use_last_name, use_name, use_password, use_phone,
                   use_username, use_role, use_audit_cd, use_audit_md)
values (1, '2010-01-02', 'admin@example.com', 'UNKNOWN', 'admin', 'admin',
        '$2a$10$iN1NlTwea59/dYa1ubrd.uzam6XxAdMWNgM2.66JbZTx4.5GmM9he', '+48 529 444 333', 'admin', 'ADMIN',
        current_timestamp, current_timestamp);